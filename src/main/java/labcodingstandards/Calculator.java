// Copyright (C) 2020
package labcodingstandards;

import java.util.Scanner;

/**
 * Calculadora de toa la vida.
 * @author Andres
 */
public class Calculator {
    //CHECKSTYLE:OFF
    /**
     * Main method to run the calculator.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        System.out.print("1. +\n2. -\n3. *\n4. /\nEnter an operator: ");
        char operator = reader.nextLine().charAt(0);
        double firstNumber;
        double secondNumber;
        String userInput;
        while (true) {
            System.out.print("Enter first number: ");
            userInput = reader.nextLine();
            try {
                firstNumber = Integer.parseInt(userInput);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Not valid!");
            }
        }
        while (true) {
            System.out.print("Enter second number: ");
            userInput = reader.nextLine();
            try {
                secondNumber = Integer.parseInt(userInput);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Not valid!");
            }
        }

        Calculator calculator = new Calculator();
        String result = calculator.operation(firstNumber, secondNumber, operator);

        System.out.printf(result);
        reader.close();
    }
    //CHECKSTYLE:ON

    /**
     * Performs the operation based on the given operator.
     * @param firstNumber the first number
     * @param secondNumber the second number
     * @param operator the operator
     * @return the result of the operation
     */
    private String operation(double firstNumber, double secondNumber, char operator) {
        double calculationResult = 0;
        switch (operator) {
            case '1':
                calculationResult = firstNumber + secondNumber;
                break;
            case '2':
                calculationResult = firstNumber - secondNumber;
                break;
            case '3':
                calculationResult = firstNumber * secondNumber;
                break;
            case '4':
                calculationResult = firstNumber / secondNumber;
                break;
            default:
                return "Error! operator is not correct";
        }
        return "The result is: " + calculationResult;
    }
}
